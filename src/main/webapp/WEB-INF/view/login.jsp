<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Log In Page</title>
    <!-- Core CSS - Include with every page -->
    <link href="<c:url value="/assets/plugins/bootstrap/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/assets/font-awesome/css/font-awesome.css"/>" rel="stylesheet" />
    <link href="<c:url value="/assets/pace/pace-theme-big-counter.css"/>" rel="stylesheet" />
    <link href="<c:url value="/assets/css/style.css"/>" rel="stylesheet" />
    <link href="<c:url value="/assets/css/main-style.css"/>" rel="stylesheet" />

</head>

<body class="body-Login-back">

    <div class="container">
       
        <div class="row">
<!--             <div class="col-md-4 col-md-offset-4 text-center logo-margin ">
              <img src="assets/img/logo.png" alt=""/>
                </div>
 -->            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">                  
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="/SampleDashboard/dash" method="POST">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="uname" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <!-- Core Scripts - Include with every page -->
     <script src="<c:url value="/assets/plugins/jquery-1.10.2.js"/>"></script>
    <script src="<c:url value="/assets/plugins/bootstrap/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/assets/plugins/metisMenu/jquery.metisMenu.js"/>"></script>
</body>

</html>
