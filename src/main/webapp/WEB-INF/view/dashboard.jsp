<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <!-- Core CSS - Include with every page -->
   <!--  <script src="https://maps.googleapis.com/maps/api/js?allback=initAutocomplete" async defer></script> -->
   <!--  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> -->
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link href="<c:url value="/assets/css/main.css" />" rel="stylesheet">
    <link href="<c:url value="/assets/plugins/bootstrap/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/assets/font-awesome/css/font-awesome.css"/>" rel="stylesheet" />
    <link href="<c:url value="/assets/pace/pace-theme-big-counter.css"/>" rel="stylesheet" />
    <link href="<c:url value="/assets/css/style.css"/>" rel="stylesheet" />
    <link href="<c:url value="/assets/css/main-style.css"/>" rel="stylesheet" />
    <link href="<c:url value="/assets/map/map.css"/>" rel="stylesheet" />
    <!-- Page-Level CSS -->
    <link  href="<c:url value="/assets/plugins/morris/morris-0.4.3.min.css"/>" rel="stylesheet" />
     
 <script type="text/javascript">
  
//Set timeout variables.
  setInterval("my_function();",1000); 
  	var j=1;
    function my_function(){
        //window.location = location.href;
        	$('#myPopup').html(j);
        	j=j+1;
        	if(j>5){
        		j=1;
        	}
    }
    
    function upload(e){
    	 var content='';
    	$.ajax({
			type : "GET",
			contentType : "application/json",
			url : "getData",
			data : '',
			dataType : 'json',
			timeout : 100000,
			success : function(data) {
				console.log("SUCCESS: ", data);
				//alert("data "+JSON.stringify(data));
				content='<table id="testTable" class="table table-bordered table-hover table-striped">';
				content+='<thead>';
				content+='<th>First Name</th>';
				content+='<th>Last Name</th>';
				content+='<th>City</th>';
				content+='</thead>';
				content+='<tbody>';
				content+='<tr><td>'+data.firstName+'</td>';
				content+='<td>'+data.lastName+'</td>';
				content+='<td>'+data.city+'</td>';
				content+='</tr>';
				content+='</tbody>'
				content+='</table>';
				$('#testTable').html(content);
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
		}); 
    }
   
	
  </script>  
    
</head>
<body>
    <!--  wrapper -->
    <div id="wrapper">
        <!-- navbar top -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbar">
            <!-- navbar-header -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="./login">
                    <!-- <img src="assets/img/logo.png" alt="" /> -->
                </a>
            </div>
            <!-- end navbar-header -->
            <!-- navbar-top-links -->
            <ul class="nav navbar-top-links navbar-right">
                <!-- main dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <span class="top-label label label-warning" id="myPopup"></span><i class="fa fa-bell fa-3x"></i>
                    </a>
                    <!-- dropdown alerts-->
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i>New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i>3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i>Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i>New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i>Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- end dropdown-alerts -->
                </li>

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-3x"></i>
                    </a>
                    <!-- dropdown user-->
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i>User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i>Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="./"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                        </li>
                    </ul>
                    <!-- end dropdown-user -->
                </li>
                <!-- end main dropdown -->
            </ul>
            <!-- end navbar-top-links -->

        </nav>
        <!-- end navbar top -->

        <!-- navbar side -->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <!-- sidebar-collapse -->
            <div class="sidebar-collapse">
                <!-- side-menu -->
                <ul class="nav" id="side-menu">
                    <li>
                        <!-- user image section-->
                        <div class="user-section">
                            <div class="user-section-inner">
                                <img src="assets/img/user.jpg" alt="">
                            </div>
                            <div class="user-info">
                                <div>Subrat <strong>Das</strong></div>
                            </div>
                        </div>
                        <!--end user image section-->
                    </li>
                    <li class="sidebar-search">
                        <!-- search section-->
                        <div class="input-group custom-search-form"></div>
                        <!--end search section-->
                    </li>
                    <li class="selected">
                        <a href="./dash"><i class="fa fa-dashboard fa-fw"></i>Dashboard</a>
                    </li>
                </ul>
                <!-- end side-menu -->
            </div>
            <!-- end sidebar-collapse -->
        </nav>
        <!-- end navbar side -->
        </div>
        <!--  page-wrapper -->
        <div id="page-wrapper">

            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!--End Page Header -->
            </div>

            <div class="row">
                <div class="col-lg-8">

 					<!--Data Table-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i>Data Table
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="pull-left">
                                    	  <span><input type="file" name="uploadFile1" id="uploadFile" onchange="upload(this);"/></span>
                                    </div>
									<div class="pull-right">
										<a href="<c:url value="/assets/img/ee.jpg"/>" download>
											  <img border="0" src="<c:url value="/assets/img/ee.jpg"/>" alt="download" width="100" height="20">
											</a>
									</div>
									<div id="testTable" class="table-responsive"></div>
                                </div>

                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!--End Data Table -->

                    <!--Area chart example -->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i>Area Chart Example
                            <!-- <div class="pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a href="#">Action</a>
                                        </li>
                                        <li><a href="#">Another action</a>
                                        </li>
                                        <li><a href="#">Something else here</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a>
                                        </li>
                                    </ul>
                                </div>
                            </div> -->
                        </div>

                        <div class="panel-body">
                            <div id="morris-area-chart"></div>
                        </div>

                    </div>
                    <!--End area chart example -->

                </div>


            </div>

           <div class="row">
                <div class="col-lg-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i>Donut Chart Example
                        </div>
                        <div class="panel-body">
                            <div id="morris-donut-chart"></div>
                            <a href="#" class="btn btn-default btn-block">View Details</a>
                        </div>

                    </div>
                </div>
            </div> 
        </div>
        <!-- end page-wrapper -->

    <!-- end wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="<c:url value="/assets/plugins/jquery-1.10.2.js"/>"></script>
    <script src="<c:url value="/assets/plugins/bootstrap/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/assets/plugins/metisMenu/jquery.metisMenu.js"/>"></script>
    <script src="<c:url value="/assets/plugins/pace/pace.js"/>"></script>
    <script src="<c:url value="/assets/scripts/siminta.js"/>"></script>
    <!-- Page-Level Plugin Scripts-->
    <script src="<c:url value="/assets/plugins/morris/raphael-2.1.0.min.js"/>"></script>
    <script src="<c:url value="/assets/plugins/morris/morris.js"/>"></script>
    <script src="<c:url value="/assets/scripts/dashboard-demo.js"/>"></script>
    <%-- <script src="<c:url value="/assets/scripts/map.js"/>"></script> --%>

</body>

</html>
