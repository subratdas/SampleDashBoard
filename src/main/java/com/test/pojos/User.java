package com.test.pojos;

public class User {
  public User() {}
  
  public String getFirstName() { return firstName; }
  
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  
  public String getLastName() { return lastName; }
  
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  
  public String getCity() { return city; }
  
  public void setCity(String city) {
    this.city = city;
  }
  
  private String firstName;
  private String lastName;
  private String city;
  public String toString()
  {
    return "name " + firstName + " lastname " + lastName;
  }
}