package com.test.controller;

import com.test.pojos.User;

@org.springframework.web.bind.annotation.RestController
public class DataController
{
  public DataController() {}
  
  @org.springframework.web.bind.annotation.RequestMapping(value={"/getData"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public User getData()
  {
    System.out.println("getData");
    
    User user = new User();
    user.setFirstName("Subrat");
    user.setLastName("Das");
    user.setCity("BBSR");
    return user;
  }
}