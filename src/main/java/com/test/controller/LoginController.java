package com.test.controller;

import org.springframework.stereotype.Controller;

@Controller
public class LoginController {
  public LoginController() {}
  
  @org.springframework.web.bind.annotation.RequestMapping(value={"/"})
  public String doLogin() {
	  System.out.println("redirect to login page");
    return "login";
  }
}