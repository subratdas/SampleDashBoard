package com.test.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@org.springframework.stereotype.Controller
@RequestMapping({"/"})
public class DashboardController {
  public DashboardController() {}
  
  @RequestMapping(value={"/dash"})
  public String showDashboard(@RequestParam("uname") String username, @RequestParam("password") String password) {
	  if(("subrat".equalsIgnoreCase(username)) && ("das".equalsIgnoreCase(password))){
		  return "dashboard";
	  }else{
		  return "error";
	  }
  }
  
}